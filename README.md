**NOTE: it is HIGHLY RECOMMENDED to use [RunTools](https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/ebysusruntools_py) instead of ReadVars. RunTools is easier to install (i.e. requires no external packages), and provides more powerful and cleaner code.** For updating old code to remove ReadVars dependence, see the top of [ReadVars.py](https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/readvars_py/-/blob/master/ReadVars.py), which has suggestions for how to replace ReadVars functions with RunTools functions.

# ReadVars

python routines to read and easily plot Ebysus data; also saves data via h5py so it can be re-read quickly.  

Please see the [ReadVars wiki](https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/readvars_py/-/wikis/home) for a more detailed description about this code and what it can do!


## GETTING STARTED:  
Note: if you have not yet installed (my custom) PythonQOL codes (the module is named `QOL`), you will need to install those. You can find them here: https://github.com/Sevans711/PythonQOL  
After that, you can proceed with the steps below.

### To copy this repository to your computer do (in terminal / command line):  
```
cd Dir   
git clone https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/readvars_py
```
_(Replace `Dir` with the directory you want this folder to go in.  
This command will create a folder Dir/readvars_py and then put the repository contents in that folder.)_

### To "install" the files:
#### "pip install" method. (Recommended)
```
cd readvars_py
pip install -e .
```
This has the benefit of being relatively simple, and also if you ever want to uninstall you can just type `pip uninstall ReadVars`.  
If you have no intentions of ever editting this code, you can drop the `-e` part and just do `pip install .`. Explanation:  
The `-e` flag puts your installation in development mode, as per `python setup.py develop`. Basically, "development mode" means that if you make your own changes to this package, the changes will apply the next time you `import ReadVars`. Without development mode, you need to `pip install` the editted package every time you make edits in order for them to take effect.

#### Other methods
(Not provided here.)

#### _Troubleshooting:_
- Ensure you have all the appropriate dependencies installed. (see below)
- Try relaunching your python shell/compiler after you have done the installation steps above.

### To start using the files:
Run this code to get started:
```python
import ReadVars as rv
```

### To learn what's inside the files, here are some options:
- Look at the code itself, in `ReadVars.py`
- Use `help(obj)` to read the documentation inside the code. For example:
  - `help(rv)` will print info about all the functions inside `ReadVars` _(once you have done `import ReadVars as rv`)_.
  - `help(rv.load_var)` will print info about the `load_var` function from `ReadVars`.
  
## DEPENDENCIES:  
You will need to install `QOL`, a module for quality of life python codes.  
For that, follow this link to see the repository and instructions for installation: https://github.com/Sevans711/PythonQOL  
(For simplicity I tried to make ReadVars independent of these QOL codes but there was too much copying & pasting involved.... Sorry about the extra step!)

You may need to install `h5py`.  
For that I direct you to http://docs.h5py.org/en/stable/build.html#  

_(TODO: add dependencies appropriately to setup.py so they are installed automagically)_
