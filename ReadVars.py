#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 10:25:17 2020

@author: Sevans

--------------------------------------------------------------------------------------------------

NOTE TO NEW USERS:
IT IS RECOMMENDED TO INSTEAD USE RUNTOOLS:
https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/ebysusruntools_py
THAT PACKAGE IS MORE SOPHISTICATED/CLEAN/USEFUL THAN THE CODES IN THIS FILE.

Translating old code with ReadVars to use RunTools instead?
-- Here are some suggestions of what to use, instead of existing ReadVars functions:
rv.default_ntf      | see instead: RunTools.inspect.iQOL.get_snaps
rv.load_var         | use instead: helita.sim.ebysus.EbysusData(snapname, fast=True)  #fast=True makes reading much faster.
                    |      or use: RunTools.inspect.persistent_data.PersistentData(snapname, fast=True)
rv.varplot          | see instead: RunTools.inspect.inspect.inspect_varsTime
                    |          or: RunTools.inspect.plots.plot_dims
rv.params_from_file | see instead: RunTools.loadfiles.LoadFile(filename).values()
rv.meanT            | see instead: numpy.mean(arr, axis=(0,1,2))
rv.stats, varstats  | see instead: RunTools.inspect.stats.full_stats, RunTools.inspect.stats.classify
rv.save_inputs      | see instead: RunTools.runtools (because a main idea of RunTools is:
                    |                 make one folder per run, and don't delete runfolders containing relevant data.)


CHANGELOG TO READVARS, SINCE RUNTOOLS WAS DEVELOPED:
- ReadVars no longer breaks if QOL is not installed; instead, just functions which use QOL will not work.
- added note to new users.

--------------------------------------------------------------------------------------------------

File to read and store variables to reduce runtime of code during analysis,
by eliminating the need to dd.get_varTime() multiple times.
Mainly useful for reading data over multiple snapshots,
i.e. with get_varTime(), since get_varTime() is relatively slow.

DEPENDENCIES:
numpy
os
matplotlib
QOL.files
    h5py
QOL.plots
    scipy
    QOL.codes
functools

Probable required installations (if you haven't installed them yet):
    PythonQOL (for QOL.files, QOL.plots, QOL.codes)
        see: https://github.com/Sevans711/PythonQOL
    h5py
        see: https://docs.h5py.org/en/latest/build.html
    
FURTHER INFO ABOUT THIS FILE:
Recommended when performing tests and amount of information is relatively small,
otherwise the extra runtime could be preferable to the space it would take to store info.

Main functionality of this file is the function load_var.
It has a bunch of documentation, I will let the documentation speak for itself.

Even if storage space is low and you don't want to save anything extra, it might
still be convenient to use load_var in order to get multiple things read into
one place.

EXAMPLE:
To read ux11, ux12, ux21, ux22, all at once, and track them more easily:
    udict = load_var(dd, 'u', axes=['x'], ispecs=[1,2], ilvls=[1,2])
Then udict will store various information, including:
    the data: access via udict['ux11'], udict['ux12'], udict['ux21'], udict['ux22']
    time array of simulation: access via udict['t_arr']
    units for u, units for t: access via udict['u_u'], udict['u_t']
    filename where data was saved, if applicable: access via udict['filename']
To avoid saving the output, just set save=False. (load_var(..., ..., save=False))
"""

#TODO: add interp2 method to this file (for making imshow plots with non-uniform z.)
#TODO: implement flag in load_var:
    #to set ntf and nt0 based on existing saved files if data is already loaded.
#TODO (low priority): implement in load_var:
    #read from already existing file if possible.
    #e.g. if reading u_1-200, but file with u_1-40 already exists,
    #get data 1-40 from there and then only read u_41-200.
##TODO: flag for SI units (dd.uni.usi_(var)) in load_var
#TODO: naming scheme for files which reflects when custom snaps list is entered.

#var * dd.params['u_<unit>'] -> var in cgs units

# import builtin modules
import os
from shutil import copyfile as shutil_copyfile #only used for save_inputs()
from functools import wraps

# import external public "common" modules
import numpy as np
import matplotlib.pyplot as plt #used only for varplot

# import external public "uncommon" modules - on failure, reduce functionality but don't break.
try:
    import QOL.files as fqol
except ImportError:
    fqol = ImportError('failed to import QOL.files')
try:
    import QOL.plots as pqol        #used only for varplot
except ImportError:
    pqol = ImportError('failed to import QOL.plots')


## SPECIES / LEVELS SHORTHAND DICTS ## #for convenience.
i11 = dict(mf_ispecies=1, mf_ilevel=1)
i12 = dict(mf_ispecies=1, mf_ilevel=2)
i21 = dict(mf_ispecies=2, mf_ilevel=1)
i22 = dict(mf_ispecies=2, mf_ilevel=2)
j11 = dict(mf_jspecies=1, mf_jlevel=1)
j12 = dict(mf_jspecies=1, mf_jlevel=2)
j21 = dict(mf_jspecies=2, mf_jlevel=1)
j22 = dict(mf_jspecies=2, mf_jlevel=2)

def SL(dd):
    """returns dd.mf_[(ispecies, ilevel), (jspecies, jlevel)]"""
    return [(dd.mf_ispecies, dd.mf_ilevel), (dd.mf_jspecies, dd.mf_jlevel)]

def set_SL(dd, SL, Verbose=True):
    """sets dd species and levels to those in mfSL == [(ispec, ilvl), (jspec, jlvl)]."""
    dd.set_mfi(SL[0][0], SL[0][1])
    dd.set_mfj(SL[1][0], SL[1][1])
    if Verbose: print("Set [(ispec, ilvl), (jspec, jlvl)] to ",SL,".", sep='')
    
def file_id(filename, nt0, ntf):
    """returns 'id' of file, i.e. returns filename+str(nt0)+'-'+str(ntf)."""
    return filename+str(nt0)+"-"+str(ntf)

def default_filename(dd, var, freq=1):
    f = dd.fdir
    if f=='.': f=os.getcwd()
    return f.split('/')[-1] + "_" + var + ('' if freq==1 else '_f'+str(freq)+'_')

def snap_n(filename):
    """retrieve snap number from filename"""
    n = filename.split('_')[-1].split('.')[0]
    return -1 if not n.isdigit() else int(n)
               
def default_ntf(dd):
    """returns number of last snapshot saved, based on folder with snapshot data."""
    return max([snap_n(x) for x in os.listdir(dd.fdir) if (
        (x.startswith(dd.root_name+'_')) and (
            not x.endswith('wiki.idl')) and (x.endswith('.idl')))])

def _maintainSL(func):
    """decorator which restores species and level info to its original state.
    
    func should be a function with dd (eb.EbysusData object) as its first argument.
    _maintainSL will check species & level info of dd, then do func,
    then restore species & level info of dd to its original values.
    
    Example
    -------
    @_maintainSL
    def load_var(dd, var, **kwargs):
        #do stuff
        return #some result
    #now even if load_var raises an error, species & level info of dd will be reset.
    """
    @wraps(func) #for maintaining documentation of inner function.
    def wrapper(dd, *args, **kwargs):
        sl_init = SL(dd)
        try:
            value = func(dd, *args, **kwargs)
            set_SL(dd, sl_init, Verbose=False)
            return value
        except KeyboardInterrupt:
            print("Keyboard Interrupt. Raising Error...")
            raise
        except:
            print("Error encountered for [(ispec, ilvl), (jspec, jlvl)] ==",SL(dd))
            print("Resetting species/level data to",sl_init)
            set_SL(dd, sl_init, Verbose=True)
            raise
    return wrapper

@_maintainSL
def load_var(dd, var, nt0=1, ntf=None, axes=["x","y","z"], ispecs=[1,2], ilvls=[1,2],
             jspecs=None, jlvls=None, do_same=True, ijskips=[], fskips=[],
             units=["default"], save=True, read=True, freq=1, fast=True,
             snaps=None, panic=False,
             d="data", overwrite=False, verbose=True):
    """returns a dict of var<axis><ispec><ilvl>, by reading dd from nt0 to ntf.
    
    (Compatibility: specs & lvls keywords have been renamed to ispecs and ilvls.)
    Use ispecs=None to use ispec=1, ilvl=1, and hide ispec & ilvl from key names.
    By default, saves info so that it can be quickly re-read later.
    
    Data is saved to match with data in snapshots, i.e. it has ebysus units.
    (load_var data * dd.uni.<relevant_unit> = load_var data in cgs.)
    (snapshot data * dd.uni.<relevant_unit> = snapshot data in cgs.)
    
    Returns a dict, with keys:
    --------------------------
    var<axis> : array #[data in simulation units].
        only if ispecs or ilvls is None or [].
        Values correspond to ispec=1, ilvl=1.
        e.g. 'bx' (axis="x").
    var<axis><ispec><ilvl> : array #[data in simulation units].
        only if jspecs or jlvls is None or [].
        Values correspond to jspec=1, jlvl=1.
        e.g. 'ux21' (axis="x",ispec=2,ilvl=1); 'mfe_tg11' (axes=[""],ispec=1,ilvl=1).
    var<axis><ispec><ilvl>_<jspec><jlvl> : array #[data in simulation units].
        only if ispecs, ilvls, jspecs, jlvls are all not None or [].
        e.g. 'nu_ij12_34' (axes=[""],ispec=1,ilvl=2,jspec=3,jlvl=4)
    t_arr : array
        time [in simulation units].
    u_var, u_t : floats
        values to multiply var & time to convert to cgs, respectively.
    filename : string
        if applicable; name of file where info is stored.
    snaps : list of integers
        snapshot numbers which were read.
    var : string
        equal to <var> which is input to this function.
    
    Parameters
    ----------
    dd : EbysusData().
    
    var : string
        the name of the variable to read
    nt0 : int. Default: 1
        start index for dd.get_varTime(snap = ____)
    ntf : int or None. Default: None
        stop index for dd.get_varTime(snap = ____)
        if None, set to rv.default_ntf(dd).
    axes : list of strings, or [""]. Default: ["x", "y", "z"]
        Appends to variable name to read. 
        e.g. var="var" with axes=["x","y"] will read "varx" and "vary".
    ispecs : list of integers, None, or []. Default: [1,2]
        Reads var for each ispecies in specs.
        if ispecs or ilvls is None or [], do not iterate over ispecies/levels
    ilvls : list of integers, None, or []. Default: [1,2]
        Reads var for each ilevel in ilvls.
    jspecs : list of integers, None, or []. Default: None
        Reads var for each ispecies in specs.
        if jspecs or jlvls is None or [], do not iterate over jspecies/levels
    jlvls : list of integers, None, or []. Default: None
        Reads var for each ilevel in ilvls.
    do_same : bool. Default: True
        whether to read when (is,il) == (js,il). 
        e.g. if do_same==False, will skip (is,il,js,jl)==1111 or 1212 or 2121 or 2222.
    ijskips : List of 4-tuples. Default: []
        combinations of (is,il,js,jl) to skip.
        e.g. if ijskips=[(1,2,3,4)] will skip ispec=1,ilvl=2,jspec=3,jlvl=4.
        Applies after do_same is considered.
    fskips : List of 2-tuples. Default: []
        combinations of (specie, level) to skip.
        e.g. if fskips=[(3,4),(2,2)] will ignore species (2,2) and (3,4).
    units : list of "units" data to record. Default: ["default"]
        List of which named units to record (store as key:value in result).
        If "default" is in list, also records u_<var> if var is in dd.params.keys().
    save : False, True, or string. Default True
        False -> does not save.
        True -> saves to default filename:
        filename -> saves to filename + str(nt0) + '-' + str(ntf)
        If filename is passed without a leading '/', saves in fqol.h5dir folder.
        If '^' is in filename, replaces '^' with var.
    read : bool. Default True
        whether to read var if it already exists in location determined by <save> input.
        If save is False, will not read.
        If save is True, check rv.file_id(rv.default_filename(dd, var), nt0, ntf).
        If save is string, check rv.file_id(save, nt0, ntf)
    freq : positive integer. Default 1
        Frequency for snapshot reading. freq=N -> read every Nth snapshot.
    fast : True, False. Default True
        If True, set dd.fast=True.
        This makes reading variables faster, by reading only the necessary variables.
    snaps : list or None. Default None.
        snapshots to read. If None or [], changes to: np.arange(nt0, ntf, freq).
        If not None or [], sets freq to 1, nt0 to min(snaps), ntf to max(snaps).
    panic : True, False. Default False.
        Whether to read the panic snapshot (get_var(var, panic=True)), if it exists.
        NOT YET COMPATIBLE WITH VARPLOT - NOT FULLY IMPLEMENTED.
    d : string
        name of base directory in h5py savefile. 
    overwrite : bool
        whether to overwrite existing data in savefile location.
    verbose : bool
        whether to print info about which variables are being read.
    """
    
    ###Adjust freq, nt0, and ntf if snaps is not default.
    if not (snaps is None or len(snaps)==0):
        SNAPS_TO_READ = np.array(snaps)
        nt0 = np.min(SNAPS_TO_READ)
        ntf = np.max(SNAPS_TO_READ)
        
        ##output string (print with end='')
        MAX_PRINTABLE_SNAPS_LEN = 15
        if not verbose:
            PRINTSTR = ''
        elif len(SNAPS_TO_READ) <= MAX_PRINTABLE_SNAPS_LEN:
            PRINTSTR = 'reading {:s} from snaps '+str(snaps)+'\n'
        else:
            PRINTSTR = 'reading {:s} from custom snap list '+ \
                    '(min={:d}, max={:d}, Nsnaps={:d})\n'.format(nt0, ntf, len(SNAPS_TO_READ))
        
    else:
        ###Set ntf to default if it was not entered.
        if ntf is None:
            ntf = default_ntf(dd)
            
        ###Set SNAPS_TO_READ based on nt0, ntf, and freq.
        SNAPS_TO_READ = np.arange(nt0, ntf+1, freq)
        
        ##output string (print with end='')
        PRINTSTR = '' if not verbose \
                else 'reading {:s} from snaps ' + \
                    '{:d}, {:d}, ..., {:d}\n'.format(nt0, nt0+freq, ntf)
        
    ###Set Axes to default if it was None or []
    axes = [""] if (axes is None or axes==[]) else axes    
    
    ###Determine savefile name. If it already exists, read data from it.
    savefile = None if not save \
                    else ( save.replace('^', var) if type(save)==str \
                          else default_filename(dd, var, freq=freq) )
    savefile = None if savefile is None else file_id(savefile, nt0, ntf)
    
    if (read) and (savefile is not None):
        u = fqol.read(savefile, d=d)
        if u is not None:
            print(d,"read from",savefile)
            return u #Exits load_var function entirely.
    
    ###Define var_id naming function based on which info was entered to load_var.
    def specs_input(specs, lvls):
        return not (specs is None or lvls is None or specs==[] or lvls==[])
    
    if specs_input(jspecs, jlvls):
        if not specs_input(ispecs, ilvls):
            print("Error, jspecs & jlvls not implemented without ispecs & ilvls.")
            print("Please enter ispecs and ilvls, or do not enter jspecs and jlvls.")
            return #Exits load_var function entirely.
        else:
            def var_id(axis, ispec, ilvl, jspec, jlvl):
                return var+axis+str(ispec)+str(ilvl)+'_'+str(jspec)+str(jlvl)
    else:
        jspecs=[1]; jlvls=[1]
        if specs_input(ispecs, ilvls):
            def var_id(axis, ispec, ilvl, jspec, jlvl):
                return var+axis+str(ispec)+str(ilvl)
        else: 
            ispecs=[1]; ilvls=[1]
            def var_id(axis, ispec, ilvl, jspec, jlvl):
                return var+axis
    
    ###Determine is,il,js,jl combinations to skip, if any.
    ijskips = [] + ijskips #copy made to maintain input.
    if not do_same:
        spec_same = [s for s in ispecs if s in jspecs]
        lvl_same  = [l for l in ilvls  if l in jlvls ]
        for s in spec_same:
            for l in lvl_same:
                ijskips += [(s,l,s,l)]
    
    ###Change dd to fast mode if fast=True
    if fast:
        dd.fast=True
    
    ###Main Body of load_var function, if it has to do any difficult work.
    u=dict()
    for axis in axes:
        for ispec in ispecs:
            for ilvl in ilvls:
                for jspec in jspecs:
                    for jlvl in jlvls:
                        if (ispec,ilvl,jspec,jlvl) in ijskips: continue
                        if (ispec,ilvl) in fskips: continue
                        if (jspec,jlvl) in fskips: continue
                        getname = var + axis
                        keyname = var_id(axis, ispec, ilvl, jspec, jlvl)
                        print(PRINTSTR.format(keyname), end='')
                        u[keyname] = dd.get_varTime(getname, snap=SNAPS_TO_READ, panic=panic,
                                                    mf_ispecies=ispec, mf_ilevel=ilvl,
                                                    mf_jspecies=jspec, mf_jlevel=jlvl)
    u["var"]   = var
    u["t_arr"] = dd.params['t']
    if nt0==0: u["t_arr"][0]=0.0 
    u["u_t"]   = dd.params['u_t']
    u["snaps"] = np.append(SNAPS_TO_READ, -2) if panic else SNAPS_TO_READ
    
    ###Add units to dict
    if "default" in units:
        if "u_"+var in dd.params.keys():
            u["u_"+var] = dd.params["u_"+var] #put default units in u.
        units = [uni for uni in units if uni!="default"] #remove "default" from units list.
    for uni in units:
        if "u_"+uni in dd.params.keys(): u["u_"+uni] = dd.params["u_"+uni]
        else: print("did not find key",uni,"in dd.params.keys()")
    ###Save & return
    if savefile is not None:
        u["filename"] = savefile
        if overwrite: print("overwrite=True is not yet fully tested")
        fqol.write(u, savefile, d=d, overwrite=overwrite)
        
    return u

def load_u(dd, **kwargs):
    return load_var(dd, 'u', **kwargs)



## Plotting the Output of load_var() ##

def varplot(d, units='default', dsd=dict(), std=None, **kwargs):
    """plots d (dict in format of load_var() result) in cgs units.
    
    Stylizes unless stylize_keys is passed as kwarg.
    
    units : string or number. default: "default"
        if   "default",  units assumed to be d["u_"+var].
        if other string, units assumed to be d["u_"+uni].
        if number, units assumed to be uni.
    dsd : dict. default: dict() #empty dict
        passed as **dsd to _default_style for stylization of keys.
    **kwargs : passed to pqol.dictplot.
        if yfunc or xfunc in kwargs, converts units then runs those functions.
    """
    var = d["var"]
    uni = d["u_"+var].mean()   if units=="default" else \
          d["u_"+units].mean() if type(units)==str else \
          units #if uni is a number.
    stylize = _default_style(var, **dsd)
    stylize += kwargs.pop('stylize_keys', [])
    yf = kwargs.pop('yfunc', lambda y: y)
    xf = kwargs.pop('xfunc', lambda x: x)
    p = pqol.dictplot("t_arr", d, 
                      yfunc=lambda y: yf(mean2d(y) * uni),
                      xfunc=lambda x: xf(x * d["u_t"].mean()),
                      stylize_keys = stylize,
                      **kwargs)
    plt.xlabel("t [s]");
    return p

def _default_style(var='*', colors='default', ls=['-','--','-.',':'],
                   cprop=['^x*','^z*','^y*'], lprop=['*11','*12','*21','*22'],
                   alphas=[0.5], aprop=['*_std']):
    """returns default stylize_keys to use in dictplot.
    
    colors = colors to apply to cprop.
    ls = linestyles to apply to lprop.
    cprop/lprop strings: var will get color/ls if strmatch(key, prop).
    ^ is to be replaced with var; * is a wildcard and can only be at start/end."""
    colors = colors if colors!='default' else pqol.Nth_color(range(len(cprop)))
    
    ssc = [cp.replace('^', var) for cp in cprop]
    sdc = [dict(color=c) for c in colors]
    ssl = [lp.replace('^', var) for lp in lprop]
    sdl = [dict( ls  =l) for l in   ls  ]
    ssa = [ap.replace('^', var) for ap in aprop]
    sda = [dict(alpha=a) for a in alphas]
    
    result = []
    for i in range(min(len(sdc), len(ssc))):
        result += [ssc[i]]
        result += [sdc[i]]
    for i in range(min(len(sdl), len(ssl))):
        result += [ssl[i]]
        result += [sdl[i]]
    for i in range(min(len(sda), len(ssa))):
        result += [ssa[i]]
        result += [sda[i]]
    return result

### File manipulation ###
## Read idl file for snapshot or panic or scratch ##

def params_from_file(filename, comment_str=';'):
    """returns a dict of all the params from file. (e.g. file = mhd.in or a snapshot file.)
    
    output will all be in strings; you will still need to convert to value by hand.
    e.g. result['nsnap'] = '100'. Convert to integer via int(result['nsnap'])
    """
    with open(filename) as f:
        lines = f.read()
    lines = lines.split('\n')                                 #splits into lines
    lines = [s.split(comment_str)[0] for s in lines]          #ignores comments
    lines = [s.split('=') for s in lines]    #splits into left and right side of equals sign
    lines = [x for x in lines if len(x)==2]  #discards any lines not like "a = b"
    lines = [[s.lstrip().rstrip() for s in x] for x in lines] #removes whitespace from sides
    return {line[0] : line[1] for line in lines}

## Save Input Files ##
def save_inputs(extra_files=[], files=['mhd.in', 'make_mf_snap.py', 'mf_params.in'], folder='INPUT', force_same_folder=False, verbose=True):
    """saves files to a folder INPUT in pqol.save_dir.
    
    add files to save via <extra_files>.
    saves by default: files=['mhd.in', 'make_mf_snap.py', 'mf_params.in'].
    saves to: os.path.join(pqol.savedir, <folder>). Default <folder>: "INPUT".
    
    By default, does not overwrite existing INPUT save. (e.g. writes to "1- INPUT" instead.)
    Set force_same_folder=True to make it instead overwrite existing input.
    """
    save_these_files = np.unique(np.append(extra_files, files))
    if verbose: maxfilestrsize = np.max([len(s) for s in save_these_files])
    INPUT_FOLDER_DIR = pqol.savedir
    if force_same_folder: INPUT_FOLDER_NAME = os.path.join(INPUT_FOLDER_DIR, folder)
    else: INPUT_FOLDER_NAME = pqol._convert_to_next_filename("INPUT", INPUT_FOLDER_DIR)
    if not os.path.exists(INPUT_FOLDER_NAME): os.mkdir(INPUT_FOLDER_NAME)
    for savefile in save_these_files:
        src = savefile
        dst = os.path.join(INPUT_FOLDER_NAME, savefile)
        shutil_copyfile(src, dst)
        if verbose: 
            #print("saved {:"+str(maxfilestrsize)+"s} ---> {:s}")
            print(("saved {:^"+str(maxfilestrsize)+"s} ---> {:s}").format(src, dst))
    #TODO: adaptive saving: only save if it does not match previous input.
        #or save input files in a separate folder and make symlinks to them;
        #and put new files when things are changed.
        #or only save the different part of the file. (save git diff output?)

### Simple Helpful Functions ###

def meanT(u):
    """mean for u with u.shape==(m, 1, n, t). does not average over t."""
    return u.mean(axis=0).mean(axis=0).mean(axis=0)

def mean2d(u):
    """mean for u with u.shape==(m, 1, n, t). does not average over t.
    implemented for backwards compatibility. Just calls meanT. Use meanT instead.
    """
    return meanT(u)

def mu(dd, att1=1, att2=2):
    """returns mu for att1 & att2 in dd. mu = m1 m2 / (m1 + m2)"""
    m = [dd.att[i].params.atomic_weight for i in (att1, att2)]
    return m[0] * m[1] / (m[0] + m[1])

def varstats(x, units=1):
    """returns stats for each thing in x"""
    return {key: stats(x[key]*units) for key in x.keys() if key.startswith(x['var'])}

def stats(x):
    """returns dict of min, mean, max, std"""
    return {'min':np.min(x), 'mean':np.mean(x), 'max':np.max(x), 'std':np.std(x)}

def logstats(x, d=3):
    """returns dict of min, mean, max, std, formatted as '{:0.'+str(d)+'e}'."""
    r=stats(x)
    return {key: ('{:0.'+str(d)+'}').format(r[key]) for key in r.keys()}

def prettystats(x_or_stats, stat_func=stats, fmt='{:+7.2e}'):
    """returns pretty string for <stats> in a nice format. e.g. strstats(stats(x))"""
    if type(x_or_stats)==dict: s = x_or_stats
    else: s = stat_func(x_or_stats)
    return ('mean: '+fmt+' || std: '+fmt+' || min: '+fmt+' | max: '+fmt).format( \
            s['mean'], s['std'], s['min'], s['max'])

def units(u, system="u_"):
    """returns units. u should be dd.params or Bifrost_units('mhd.in').
    system is u_ for cgs units, or usi_ for SI units.
    """
    if type(u)==dict: #u is dd.params
        return {x[len(system):]:u[x][0] for x in u.keys() if x.startswith(system)}
    else: #u is Bifrost_units('mhd.in')
        return {x[len(system):]:u.__dict__[x] for x in u.__dict__.keys() if x.startswith(system)}
    
