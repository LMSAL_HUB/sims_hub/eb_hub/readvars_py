#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 12:40:21 2020

@author: Sevans
"""

from setuptools import setup

setup(
      name='ReadVars',
      version='1.0',
      py_modules=['ReadVars'],
      author   = 'Samuel Evans',
      author_email = 'sevans7@bu.edu',
      url = 'https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/readvars_py'
)